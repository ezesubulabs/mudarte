<?php 
/**
 * Plugin Name: QK Register Post Type
 * Description: This plugin register all post type come with theme.
 * Version: 1.0
 * Author: Quannt
 * Author URI: http://qkthemes.com
 */
?>
<?php


add_action( 'init', 'codex_team_init' );
/**
 * Register a Team post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function codex_team_init() {
	$labels = array(
		'name'               => __( 'Teams', 'post type general name', 'stilo' ),
		'singular_name'      => __( 'Team', 'post type singular name', 'stilo' ),
		'menu_name'          => __( 'Teams', 'admin menu', 'stilo' ),
		'name_admin_bar'     => __( 'Team', 'add new on admin bar', 'stilo' ),
		'add_new'            => __( 'Add New', 'Team', 'stilo' ),
		'add_new_item'       => __( 'Add New Team', 'stilo' ),
		'new_item'           => __( 'New Team', 'stilo' ),
		'edit_item'          => __( 'Edit Team', 'stilo' ),
		'view_item'          => __( 'View Team', 'stilo' ),
		'all_items'          => __( 'All Team', 'stilo' ),
		'search_items'       => __( 'Search Team', 'stilo' ),
		'parent_item_colon'  => __( 'Parent Team:', 'stilo' ),
		'not_found'          => __( 'No Team found.', 'stilo' ),
		'not_found_in_trash' => __( 'No Team found in Trash.', 'stilo' ),
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'menu_icon' 		 => 'dashicons-id-alt',
		'publicly_queryable' => true,
		'menu_position' 	 => 2,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'team' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'thumbnail')
	);

	register_post_type( 'team', $args );
}

add_action( 'init', 'codex_testimonial_init' );
/**
 * Register a Team post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function codex_testimonial_init() {
	$labels = array(
		'name'               => __( 'Testimonial', 'post type general name', 'stilo' ),
		'singular_name'      => __( 'Testimonial', 'post type singular name', 'stilo' ),
		'menu_name'          => __( 'Testimonial', 'admin menu', 'stilo' ),
		'name_admin_bar'     => __( 'Testimonial', 'add new on admin bar', 'stilo' ),
		'add_new'            => __( 'Add New', 'Testimonial', 'stilo' ),
		'add_new_item'       => __( 'Add New Testimonial', 'stilo' ),
		'new_item'           => __( 'New Testimonial', 'stilo' ),
		'edit_item'          => __( 'Edit Testimonial', 'stilo' ),
		'view_item'          => __( 'View Testimonial', 'stilo' ),
		'all_items'          => __( 'All Testimonial', 'stilo' ),
		'search_items'       => __( 'Search Testimonial', 'stilo' ),
		'parent_item_colon'  => __( 'Parent Testimonial:', 'stilo' ),
		'not_found'          => __( 'No Testimonial found.', 'stilo' ),
		'not_found_in_trash' => __( 'No Testimonial found in Trash.', 'stilo' ),
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'menu_icon' 		 => 'dashicons-id-alt',
		'publicly_queryable' => true,
		'menu_position' 	 => 2,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'testimonial' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'thumbnail','editor')
	);

	register_post_type( 'testimonial', $args );
}




?>