<?php

// Creating the widget 
class moving_team_widget extends WP_Widget {

function __construct() {

parent::__construct(
// Base ID of your widget
'moving_team_widget', 

// Widget name will appear in UI
esc_html__('QK Team', 'moving'), 

// Widget description
array( 'description' => esc_html__( 'Listing your Team as carousel.', 'moving' ), ) 
);
}

// Creating widget front-end
// This is where the action happens
public function widget( $args, $instance ) {
$title = apply_filters( 'widget_title', $instance['title'] );
// before and after widget arguments are defined by themes
echo $args['before_widget'];
if ( ! empty( $title ) )
echo $args['before_title'] . $title . $args['after_title'];

// This is where you run the code and display the output
?>

<div class="team">
    <div class="our-team col-md-12 top_30 bottom_90 owl-carousel" data-autoplay="5000" data-pagination="false" data-items-desktop="3" data-items-desktop-small="3" data-items-tablet="2" data-items-tablet-small="1">
       	<?php 
				
			$arr = array('post_type' => 'team', 'posts_per_page' => $instance['count']);
			$query = new WP_Query($arr);
			while($query->have_posts()) : $query->the_post();
		?>
        <!-- teammate -->
        <div class="team-friend col-md-12 item">
            <figure> <?php the_post_thumbnail(); ?>
                <figcaption> 
                    <div class="teaminf">
                    <div class="social-icons"> 
                    	<?php $social = get_post_meta(get_the_ID(), '_moving_social', true); echo do_shortcode($social);?>
                    </div>
                    <h3><?php the_title(); ?></h3>
                    <p><?php echo get_post_meta(get_the_ID(), '_moving_job', true); ?></p>
                    </div> 
                </figcaption>
            </figure>
        </div>
        <?php endwhile; ?>
		<?php wp_reset_postdata(); ?>
    </div>
</div>
<!-- team end -->


<?php

echo $args['after_widget'];
}
		
// Widget Backend 
public function form( $instance ) {

if ( isset( $instance[ 'title' ] ) ) {
$title = $instance[ 'title' ];
}
else {
$title = esc_html__( 'POPULAR POSTS', 'moving' );

}
if ( isset( $instance[ 'count' ] ) ) {
$count = $instance[ 'count' ];
}
else {
$count = 4;
}

// Widget admin form
?>
<p>
<label for="<?php echo esc_attr($this->get_field_id( 'title' )); ?>"><?php esc_html_e( 'Title:', 'moving'); ?></label> 
<input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'title' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'title' )); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
</p>
<p>
<label for="<?php echo esc_attr($this->get_field_id( 'count' )); ?>"><?php esc_html_e( 'Number of team want to display:', 'moving'); ?></label> 
<input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'count' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'count' )); ?>" type="text" value="<?php echo esc_attr( $count ); ?>" />
</p>
<?php 
}
	
// Updating widget replacing old instances with new
public function update( $new_instance, $old_instance ) {
$instance = array();
$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
$instance['count'] = ( ! empty( $new_instance['count'] ) ) ? strip_tags( $new_instance['count'] ) : '';
return $instance;
}
} // Class wpb_widget ends here

// Register and load the widget
function moving_load_widget1() {
	register_widget( 'moving_team_widget' );
}
add_action( 'widgets_init', 'moving_load_widget1' );
?>