<?php

// Creating the widget 
class moving_testimonial_widget extends WP_Widget {

function __construct() {

parent::__construct(
// Base ID of your widget
'moving_testimonial_widget', 

// Widget name will appear in UI
esc_html__('QK Testimonial', 'moving'), 

// Widget description
array( 'description' => esc_html__( 'Listing your Testimonial as carousel.', 'moving' ), ) 
);
}

// Creating widget front-end
// This is where the action happens
public function widget( $args, $instance ) {
// before and after widget arguments are defined by themes
echo $args['before_widget'];


// This is where you run the code and display the output
?>

<div class="testimonial-box owl-wrapper">
    <?php 
        if($instance){
            $count = $instance['count'];
        }else{
            $count = 4;
        }
        
        
    ?>
    <div class="owl-carousel" data-num="3">
       	<?php 

			$arr = array('post_type' => 'testimonial', 'posts_per_page' => $count);
			$query = new WP_Query($arr);
			while($query->have_posts()) : $query->the_post();
		?>
        <div class="item">
            <div class="testimonial-post">
                <?php the_post_thumbnail(); ?>
                <?php the_content(); ?>
                <h3><?php the_title(); ?></h3>
                <span><?php echo get_post_meta(get_the_ID(), '_moving_job', true); ?></span>
            </div>
        </div>

        <?php endwhile; ?>
		<?php wp_reset_postdata(); ?>
    </div>
</div>
<!-- testimonial end -->


<?php

echo $args['after_widget'];
}
		
// Widget Backend 
public function form( $instance ) {

if ( isset( $instance[ 'title' ] ) ) {
$title = $instance[ 'title' ];
}
else {
$title = '';

}
if ( isset( $instance[ 'count' ] ) ) {
$count = $instance[ 'count' ];
}
else {
$count = 4;
}

// Widget admin form
?>

<p>
<label for="<?php echo esc_attr($this->get_field_id( 'count' )); ?>"><?php esc_html_e( 'Number of testimonial want to display:', 'moving'); ?></label> 
<input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'count' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'count' )); ?>" type="text" value="<?php echo esc_attr( $count ); ?>" />
</p>
<?php 
}
	
// Updating widget replacing old instances with new
public function update( $new_instance, $old_instance ) {
$instance = array();
$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
$instance['count'] = ( ! empty( $new_instance['count'] ) ) ? strip_tags( $new_instance['count'] ) : '';
return $instance;
}
} // Class wpb_widget ends here

// Register and load the widget
function moving_load_widgett() {
	register_widget( 'moving_testimonial_widget' );
}
add_action( 'widgets_init', 'moving_load_widgett' );
?>