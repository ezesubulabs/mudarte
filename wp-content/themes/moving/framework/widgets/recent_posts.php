<?php

// Creating the widget 
class moving_post_widget extends WP_Widget {

function __construct() {

parent::__construct(
// Base ID of your widget
'moving_post_widget', 

// Widget name will appear in UI
esc_html__('QK Popular Post', 'moving'), 

// Widget description
array( 'description' => esc_html__( 'Listing your Popular Post as carousel.', 'moving' ), ) 
);
}

// Creating widget front-end
// This is where the action happens
public function widget( $args, $instance ) {
$title = apply_filters( 'widget_title', $instance['title'] );
// before and after widget arguments are defined by themes
echo $args['before_widget'];
if ( ! empty( $title ) )
echo $args['before_title'] . $title . $args['after_title'];

// This is where you run the code and display the output
?>
<div class="popular-widget widget">
<ul class="popular-list">
<?php 
                
    $arr = array('post_type' => 'post', 'posts_per_page' => $instance['count'],'orderby' => 'comment_count');
    $query = new WP_Query($arr);
    
    while($query->have_posts()) : $query->the_post();
?>
<li>
    <?php the_post_thumbnail('thumbnail'); ?>
    <div class="side-content">
        <h2><a href="<?php the_permalink(); ?>"><?php the_title();?></a></h2>
        <span><?php the_time(get_option( 'date_format' )); ?></span>
    </div>
</li>
<?php endwhile; ?>
<?php wp_reset_postdata(); ?>
</ul>
</div>
<?php

echo $args['after_widget'];
}
		
// Widget Backend 
public function form( $instance ) {

if ( isset( $instance[ 'title' ] ) ) {
$title = $instance[ 'title' ];
}
else {
$title = '';

}
if ( isset( $instance[ 'count' ] ) ) {
$count = $instance[ 'count' ];
}else {
$count = 4;
}

// Widget admin form
?>
<p>
<label for="<?php echo esc_attr($this->get_field_id( 'title' )); ?>"><?php esc_html_e( 'Title:', 'moving'); ?></label> 
<input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'title' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'title' )); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
</p>


<p>
<label for="<?php echo esc_attr($this->get_field_id( 'count' )); ?>"><?php esc_html_e( 'Number of post want to display:', 'moving'); ?></label> 
<input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'count' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'count' )); ?>" type="text" value="<?php echo esc_attr( $count ); ?>" />
</p>


<?php 
}
	
// Updating widget replacing old instances with new
public function update( $new_instance, $old_instance ) {
$instance = array();
$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
$instance['count'] = ( ! empty( $new_instance['count'] ) ) ? strip_tags( $new_instance['count'] ) : '';
return $instance;
}
} // Class wpb_widget ends here

// Register and load the widget
function moving_load_widget2() {
	register_widget( 'moving_post_widget' );
}
add_action( 'widgets_init', 'moving_load_widget2' );
?>