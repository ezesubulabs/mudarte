<?php

    /**
     * ReduxFramework Barebones Sample Config File
     * For full documentation, please visit: http://docs.reduxframework.com/
     */

    if ( ! class_exists( 'Redux' ) ) {
        return;
    }

    // This is your option name where all the Redux data is stored.
    $opt_name = "moving_options";
    
    /**
     * ---> SET ARGUMENTS
     * All the possible arguments for Redux.
     * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
     * */

    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
        // TYPICAL -> Change these values as you need/desire
        'opt_name'             => $opt_name,
        // This is where your data is stored in the database and also becomes your global variable name.
        'display_name'         => $theme->get( 'Name' ),
        // Name that appears at the top of your panel
        'display_version'      => $theme->get( 'Version' ),
        // Version that appears at the top of your panel
        'menu_type'            => 'menu',
        //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
        'allow_sub_menu'       => true,
        // Show the sections below the admin menu item or not
        'menu_title'           => esc_html__( 'Moving Options', 'moving' ),
        'page_title'           => esc_html__( 'Moving Options', 'moving' ),
        // You will need to generate a Google API key to use this feature.
        // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
        'google_api_key'       => '',
        // Set it you want google fonts to update weekly. A google_api_key value is required.
        'google_update_weekly' => false,
        // Must be defined to add google fonts to the typography module
        'async_typography'     => true,
        // Use a asynchronous font on the front end or font string
        //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
        'admin_bar'            => true,
        // Show the panel pages on the admin bar
        'admin_bar_icon'       => 'dashicons-admin-generic',
        // Choose an icon for the admin bar menu
        'admin_bar_priority'   => 50,
        // Choose an priority for the admin bar menu
        'global_variable'      => '',
        // Set a different name for your global variable other than the opt_name
        'dev_mode'             => false,
        // Show the time the page took to load, etc
        'forced_dev_mode_off' => true,
        'update_notice'        => false,
        // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
        'customizer'           => true,
        // Enable basic customizer support
        //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
        //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

        // OPTIONAL -> Give you extra features
        'page_priority'        => null,
        // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
        'page_parent'          => 'themes.php',
        // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
        'page_permissions'     => 'manage_options',
        // Permissions needed to access the options panel.
        'menu_icon'            => '',
        // Specify a custom URL to an icon
        'last_tab'             => '',
        // Force your panel to always open to a specific tab (by id)
        'page_icon'            => 'icon-themes',
        // Icon displayed in the admin panel next to your menu_title
        'page_slug'            => '',
        // Page slug used to denote the panel, will be based off page title then menu title then opt_name if not provided
        'save_defaults'        => true,
        // On load save the defaults to DB before user clicks save or not
        'default_show'         => false,
        // If true, shows the default value next to each field that is not the default value.
        'default_mark'         => '',
        // What to print by the field's title if the value shown is default. Suggested: *
        'show_import_export'   => true,
        // Shows the Import/Export panel when not used as a field.

        // CAREFUL -> These options are for advanced use only
        'transient_time'       => 60 * MINUTE_IN_SECONDS,
        'output'               => true,
        // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
        'output_tag'           => true,
        // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
            // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

        // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
        'database'             => '',
        // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
        'system_info'          => false,
        // REMOVE

        //'compiler'             => true,

        // HINTS
        'hints'                => array(
            'icon'          => 'el el-question-sign',
            'icon_position' => 'right',
            'icon_color'    => 'lightgray',
            'icon_size'     => 'normal',
            'tip_style'     => array(
                'color'   => 'light',
                'shadow'  => true,
                'rounded' => false,
                'style'   => '',
            ),
            'tip_position'  => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect'    => array(
                'show' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'mouseover',
                ),
                'hide' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'click mouseleave',
                ),
            ),
        )
    );

    // ADMIN BAR LINKS -> Setup custom links in the admin bar menu as external items.
    

    
    // Panel Intro text -> before the form
    if ( ! isset( $args['global_variable'] ) || $args['global_variable'] !== false ) {
        if ( ! empty( $args['global_variable'] ) ) {
            $v = $args['global_variable'];
        } else {
            $v = str_replace( '-', '_', $args['opt_name'] );
        }
        $args['intro_text'] = sprintf('<p>Did you know that Redux sets a global variable for you? To access any of your saved options from within your code you can use your global variable: <strong>$%1$s</strong></p>', $v );
    } else {
        $args['intro_text'] = wp_kses_post(__( '<p>This text is displayed above the options panel. It isn\'t required, but more info is always better! The intro_text field accepts all HTML.</p>', 'moving' ));
    }

    // Add content after the form.
    $args['footer_text'] = wp_kses_post(__( '<p>This text is displayed below the options panel. It isn\'t required, but more info is always better! The footer_text field accepts all HTML.</p>', 'moving' ));

    Redux::setArgs( $opt_name, $args );

    /*
     * ---> END ARGUMENTS
     */

    /*
     * ---> START HELP TABS
     */

    $tabs = array(
        array(
            'id'      => 'redux-help-tab-1',
            'title'   => esc_html__( 'Theme Information 1', 'moving' ),
            'content' => wp_kses_post(__( '<p>This is the tab content, HTML is allowed.</p>', 'moving' ))
        ),
        array(
            'id'      => 'redux-help-tab-2',
            'title'   => esc_html__( 'Theme Information 2', 'moving' ),
            'content' => wp_kses_post(__( '<p>This is the tab content, HTML is allowed.</p>', 'moving' ))
        )
    );
    Redux::setHelpTab( $opt_name, $tabs );

    // Set the help sidebar
    $content = wp_kses_post(__( '<p>This is the sidebar content, HTML is allowed.</p>', 'moving' ));
    Redux::setHelpSidebar( $opt_name, $content );


    /*
     * <--- END HELP TABS
     */
    
    
    /*
     *
     * ---> START SECTIONS
     *
     */

    /*

        As of Redux 3.5+, there is an extensive API. This API can be used in a mix/match mode allowing for


     */

    // -> START Basic Fields
    Redux::setSection( $opt_name, array(
        'title'  => esc_html__( 'General Settings', 'moving' ),
        'id'     => 'general',
        'desc'   => '',
        'icon'   => 'el el-icon-cogs',
        'fields' => array(
             array(
                'id' => 'favicon',
                'type' => 'media',
                'url' => true,
                'title' => esc_html__('Custom Favicon', 'moving'),
                'compiler' => 'true',
                //'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc' => esc_html__('Upload your Favicon.', 'moving'),
                'subtitle' => '',
                'default' => array('url' => ''),
            ),
            array(
                'id' => 'logo',
                'type' => 'media',
                'url' => true,
                'title' => esc_html__('Logo Image', 'moving'),
                'compiler' => 'true',
                //'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'desc' => esc_html__('Upload your logo.', 'moving'),
                'subtitle' => '',
                'default' => array('url' => ''),
            ), array(
                'id' => 'logo_padding',
                'title' => 'Logo Padding',
                'subtitle' => esc_html__('Please, Enter padding of Logo.', 'moving'),
                'type' => 'spacing',
                'units' => array('px'),
                'output' => array('.navbar-brand'),
                'default' => array(
                    'padding-top'     => '35px', 
                    'padding-right'   => '15px', 
                    'padding-bottom'  => '30px', 
                    'padding-left'    => '15px',
                    'units'          => 'px', 
                )
            ),
            array(
                'id'             => 'logo-height',
                'type'           => 'dimensions',
                'units'          => array( 'px' ),    // You can specify a unit value. Possible: px, em, %
                'title'          => esc_html__( 'Logo Image Max Height', 'moving' ),
                'desc'           => esc_html__( 'Set max height for your logo image', 'moving' ),
                'width'         => false,
                'output' => array('.navbar-brand img'),
                'default'        => array(
                    'height' => 30,
                )
            ),
            array(
                        'id' => 'apple_icon',
                        'type' => 'media',
                        'url' => true,
                        'title' => esc_html__('Apple Touch Icon', 'moving'),
                        'compiler' => 'true',
                        //'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                        'desc' => esc_html__('Upload your Apple touch icon 57x57.', 'moving'),
                        'subtitle' => '',
                        'default' => array('url' => ''),
                    ),
                    array(
                        'id' => 'apple_icon_57',
                        'type' => 'media',
                        'url' => true,
                        'title' => esc_html__('Apple Touch Icon 57x57', 'moving'),
                        'compiler' => 'true',
                        //'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                        'desc' => esc_html__('Upload your Apple touch icon 57x57.', 'moving'),
                        'subtitle' => '',
                        'default' => array('url' => ''),
                    ),
                    array(
                        'id' => 'apple_icon_72',
                        'type' => 'media',
                        'url' => true,
                        'title' => esc_html__('Apple Touch Icon 72x72', 'moving'),
                        'compiler' => 'true',
                        //'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                        'desc' => esc_html__('Upload your Apple touch icon 72x72.', 'moving'),
                        'subtitle' => '',
                        'default' => array('url' => ''),
                    ),
                    array(
                        'id' => 'apple_icon_114',
                        'type' => 'media',
                        'url' => true,
                        'title' => esc_html__('Apple Touch Icon 114x114', 'moving'),
                        'compiler' => 'true',
                        //'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                        'desc' => esc_html__('Upload your Apple touch icon 114x114.', 'moving'),
                        'subtitle' => '',
                        'default' => array('url' => ''),
                    ),
                    array(
                        'id' => 'phone',
                        'type' => 'text',
                        'title' => esc_html__('Phone', 'moving'),
                        //'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                        'default' => '',
                    ),
                    array(
                        'id' => 'time',
                        'type' => 'text',
                        'title' => esc_html__('Open Time', 'moving'),
                        //'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                        'default' => '',
                    ),
                    array(
                        'id' => 'quote',
                        'type' => 'text',
                        'title' => esc_html__('Quote Link', 'moving'),
                        //'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                        'default' => '',
                    ),
        )
    ) );
    
    Redux::setSection( $opt_name,array(
        'icon'      => 'el el-magic',
        'title'     => esc_html__('Styling Options', 'moving'),
        'fields'    => array(
            
            
            array(
                'id' => 'body-font2',
                'type' => 'typography',
                'output' => array('body'),
                'title' => esc_html__('Body Font', 'moving'),
                'subtitle' => esc_html__('Specify the body font properties.', 'moving'),
                'google' => true,
                'default' => array(
                    'color' => '#333',
                    'font-size' => '15px',
                    'line-height' => '28px',
                    'font-family' => "Poppins",
                    'font-weight' => '400',
                ),
            ),
            array(
                'id' => 'main-color',
                'type' => 'color',
                'title' => esc_html__('Theme main Color', 'moving'),
                'subtitle' => esc_html__('Pick theme main color (default: #ffc952).', 'moving'),
                'default' => '#ffc952',
                'validate' => 'color',
            ),
           
             array(
                'id'        => 'custom-css',
                'type'      => 'ace_editor',
                'title'     => esc_html__('Custom CSS Code', 'moving'),
                'subtitle'  => esc_html__('Paste your CSS code here.', 'moving'),
                'mode'      => 'css',
                'theme'     => 'monokai',
                'desc'      => 'Possible modes can be found at <a href="http://ace.c9.io" target="_blank">http://ace.c9.io/</a>.',
                'default'   => "#header{\nmargin: 0 auto;\n}"
            ),
                 
           
        )
    ));
    

      Redux::setSection( $opt_name, array(
        'icon' => 'el el-dribbble',
        'title' => esc_html__('Social Settinigs', 'moving'),
        'fields' => array(
            array(
                'id' => 'email',
                'type' => 'text',
                'title' => esc_html__('Your Email', 'moving'),
                //'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'default' => 'moving@tavillathemes.com',
            ),array(
                'id' => 'facebook',
                'type' => 'text',
                'title' => esc_html__('Facebook Url', 'moving'),
                //'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'default' => '#',
            ),
            array(
                'id' => 'twitter',
                'type' => 'text',
                'title' => esc_html__('Twitter Url', 'moving'),
                //'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'default' => '#',
            ),
            array(
                'id' => 'google',
                'type' => 'text',
                'title' => esc_html__('Google+ Url', 'moving'),
                //'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'default' => '#',
            ),
            array(
                'id' => 'instagram',
                'type' => 'text',
                'title' => esc_html__('Instagram Url', 'moving'),
                //'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'default' => '#',
            ),
           
            array(
                'id' => 'dribbble',
                'type' => 'text',
                'title' => esc_html__('Dribbble Url', 'moving'),
                //'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'default' => '#'
            ),
            array(
                'id' => 'behance',
                'type' => 'text',
                'title' => esc_html__('Behance Url', 'moving'),
                //'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'default' => '#'
            ),
        )
    )
    );

    Redux::setSection( $opt_name, array(
        'title'  => esc_html__( 'Footer Settings', 'moving' ),
        'id'     => 'footer',
        'desc'   => '',
        'icon'   => 'el el-th',
        'fields' => array(
             array(
                'id'       => 'footer-widgets',
                'type'     => 'select',
                'title'    => esc_html__( 'Turn ON footer widget area', 'moving' ),
                'subtitle' => '',
                'desc'     => '',
                //Must provide key => value pairs for select options
                'options'  => array(
                    'no' => 'No',
                    'yes' => 'Yes',
                ),
                'default'  => 'yes'
            ),array(
                'id' => 'footer-text',
                'type' => 'editor',
                'title' => esc_html__('Footer Text', 'moving'),
                'subtitle' => esc_html__('Copyright Text', 'moving'),
                'default' => esc_html__('Copyright 2017 All right reserved.','moving'),
            )
        )
    ) );
    
    

    /*
     * <--- END SECTIONS
     */