<?php
/*
*Template Name: Page Builder
*/
?>
<?php get_header(); ?>
		<?php if(get_post_meta($wp_query->get_queried_object_id(), '_moving_breadcrumb', true)!='off'){ ?>
		<!-- page-banner-section
			================================================== -->
		<section class="page-banner-section about">
			<div class="container">
				<div class="page-banner-box">
					<h1><?php single_post_title(); ?></h1>
				</div>
			</div>
		</section>
		<!-- End page-banner section -->
		<?php } ?>
		<?php
		while(have_posts()) : the_post();
			the_content();
		endwhile;
		?>


<?php get_footer(); ?>