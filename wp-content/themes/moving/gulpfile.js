'use strict';

/**
 * Build Process for SASS + JS
 */
process.chdir( __dirname );
const gulp = require( 'gulp' );
const include = require( 'gulp-include' );
const sass = require( 'gulp-sass' );
const path = require( 'path' );
const uglify = require( 'gulp-uglify' );
const fsCache = require( 'gulp-fs-cache' );
const concat = require('gulp-concat');
const pump = require( 'pump' );
const ngAnnotate = require( 'gulp-ng-annotate' );
const autoprefixer = require( 'gulp-autoprefixer' );
const cleanCSS = require( 'gulp-clean-css' );
const rename = require( 'gulp-rename' );
const sourcemaps = require( 'gulp-sourcemaps' );
const fs = require( 'fs' );
const util = require( 'gulp-util' );
const livereload = require('gulp-livereload');

// Build tasks


gulp.task( 'scss', compileSASS );
gulp.task( 'js', compileJS );
gulp.task( 'html', compileHtml );
gulp.task( 'default', watch );
gulp.task( 'build', [ 'sass', 'js' ] );
livereload({ start: true })

// Error handler
function next( error ) {
    if ( !error ) return;
    console.log( error );
}

function compileSASS() {
    return gulp.src('angular/views/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('main.css'))
        .pipe(gulp.dest('./dist/css/'))
        .pipe(livereload());
}

// Compile JavaScript
function compileJS() {
    var jsCache = fsCache( '.gulp-cache/js' );
    return pump( [
        sourcemaps.init(),
        gulp.src( 'angular/main.js' ),
        include( {
            hardFail: true,
            extensions: 'js'
        } ),
        ngAnnotate(),
        jsCache,
        //uglify(),
        jsCache.restore,
        sourcemaps.write( './dist/maps' ),
        gulp.dest( './dist/js' ),
        livereload()
    ], next );
}
function compileHtml() {
    return gulp.src('angular/views/**/*.html')
        .pipe(gulp.dest(''))
        .pipe(livereload());
};

// Watch files and run tasks if they change
function watch() {
    gulp.watch(['angular/views/**/*.scss'], [ 'scss' ] );
    gulp.watch(['angular/*.js', 'angular/views/**/*.js', 'angular/services/*.js'], [ 'js' ] );
    gulp.watch('angular/views/**/*.html', ['html']);

    livereload.listen();

}