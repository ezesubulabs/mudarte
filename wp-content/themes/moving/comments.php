
<?php
/**
 * The template for displaying Comments.
 *
 * The area of the page that contains comments and the comment form.
 *
 * If the current post is protected by a password and the visitor has not yet
 * entered the password we will return early without loading the comments.
 */
if ( post_password_required() )
    return;
?>

    <?php
    $aria_req = ( $req ? " aria-required='true'" : '' );
    $comment_args = array(
        'title_reply'=> esc_html__('Leave a Comment','moving'),
        'fields' => apply_filters( 'comment_form_default_fields', array(
            'author' => '
                    <div class="row">
                    <div class="col-md-4">
                    
                    
                    <input type="text" name="author" placeholder="'.esc_html__('Name','moving').'*" id="name" value="' . esc_attr( $commenter['comment_author'] ) . '" ' . $aria_req . ' /></div>
                
            ',
            'email' => '
                <div class="col-md-4">
                    
                        
                    <input id="mail" name="email" placeholder="'.esc_html__('Email','moving').'*" type="email" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" ' . $aria_req . ' /></div>
                
            ',
            'url' => '<div class="col-md-4">
                
                <input id="website" name="url" placeholder="'.esc_html__('Website','moving').'" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) . '"  /></div></div>
            
            '
        )),
        'comment_field' => '
                <textarea  id="comment"  placeholder="'.esc_html__('Comment','moving').'*" name="comment"'.$aria_req.'></textarea>',
        'label_submit' => esc_html__('Submit Comment','moving'),
        'comment_notes_after' => '',
    );
    ?>


<div class="comment-area-box">
    <h2><?php comments_number( esc_html__('0 Comments','moving'), esc_html__('1 Comment','moving'), esc_html__('% Comments','moving') ); ?></h2>
    <ul class="comment-tree">
        <?php wp_list_comments('callback=moving_theme_comment'); ?>
    </ul>
    <?php
    // Are there comments to navigate through?
    if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) :
        ?>
        <footer class="navigation comment-navigation" role="navigation">
            
            <div class="previous"><?php previous_comments_link( esc_html__( '&larr; Older Comments', 'moving' ) ); ?></div>
            <div class="next right"><?php next_comments_link( esc_html__( 'Newer Comments &rarr;', 'moving' ) ); ?></div>
        </footer><!-- .comment-navigation -->
    <?php endif; // Check for comment navigation ?>
</div>

<div class="contact-form-box">
    <?php if('open' == $post->comment_status){ ?>

        <?php comment_form($comment_args); ?>

    <?php } ?>

</div>
<!-- End contact form box -->




