<?php get_header(); $moving_custom  = moving_custom();?>

		<!-- page-banner-section
			================================================== -->
		<section class="page-banner-section blog">
			<div class="container">
				<div class="page-banner-box">

					<?php if($moving_custom != null and $moving_custom['quote']!=''){ ?>
                    <a href="<?php echo esc_url($moving_custom['quote']); ?>"><?php esc_html_e('Request a Free Quote','moving'); ?></a>
                    <?php } ?>
				</div>
			</div>
		</section>
		<!-- End page-banner section -->
		<?php while(have_posts()): the_post(); ?>
		<!-- blog-section
			================================================== -->
		<section class="blog-section">
			<div class="container">
				<div class="row">
					<div class="col-md-8">
						<div class="blog-box">

							<div class="blog-post single-post">
								<?php the_post_thumbnail();?>
								<h2><?php single_post_title(); ?></h2>
								<ul class="post-meta">
									<li>
                                        <i class="fa fa-calendar"></i>
                                        <?php the_time(get_option( 'date_format' )); ?>
                                    </li>
                                    <li>
                                        <i class="fa fa-user"></i>
                                        <?php esc_html_e('by','moving'); ?> <?php the_author_posts_link(); ?>
                                    </li>
                                    <li>
                                        <i class="fa fa-comments"></i> <?php
                                                                      comments_popup_link( esc_html__('0 comments','moving'), esc_html__('1 comment','moving'), esc_html__('% comments','moving'));
                                                                    ?>
                                    </li>
								</ul>
								<?php the_content(); ?>
		                          <?php
		                            $defaults = array(
		                              'before'           => '<div id="page-links"><strong>'.esc_html__('Page','moving').': </strong>',
		                              'after'            => '</div>',
		                              'link_before'      => '<span>',
		                              'link_after'       => '</span>',
		                              'next_or_number'   => 'number',
		                              'separator'        => ' ',
		                              'nextpagelink'     => esc_html__( 'Next page','moving' ),
		                              'previouspagelink' => esc_html__( 'Previous page','moving' ),
		                              'pagelink'         => '%',
		                              'echo'             => 1
		                            );
		                           ?>
		                          <?php wp_link_pages($defaults); ?>

		                          <?php if(has_tag()){ ?>
		                                <?php the_tags('<ul class="tag-list"><li><span>Tags:</span></li><li>', '</li><li>', '</li></ul>' ); ?>
		                        <?php } ?>
							</div>

							<?php if ( comments_open() || get_comments_number() ) {  ?>
	                            <?php comments_template(); ?>
	                    <?php } ?>

						</div>
					</div>
					<div class="col-md-4">

						<?php get_sidebar(); ?>
					</div>
				</div>
			</div>
		</section>
		<!-- End blog section -->
	<?php endwhile; ?>
<?php get_footer(); ?>