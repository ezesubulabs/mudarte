
<?php
/**
 * Search Form Template
 *
 */
?>
	<div class="search-widget">
	<form class="searchform" action="<?php echo esc_url( home_url('/') ); ?>" method="get">
		<input type="search" name="s"  placeholder="<?php esc_attr_e( 'Search', 'moving' ); ?>:"/>
		
	</form>
	</div>
