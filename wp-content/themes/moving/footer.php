	<?php $moving_custom  = moving_custom(); ?>
		<!-- footer 
			================================================== -->
		<footer>
			
				<?php 
		   		$active = 0; 
		   		
				if( is_active_sidebar( 'footer-1' ) ){
					$active = $active+1;
				}
				
				if( is_active_sidebar( 'footer-2' ) ){
								$active = $active+1;
				}
				
				if( is_active_sidebar( 'footer-3' ) ){
					$active = $active+1;
				}
				
				if( is_active_sidebar( 'footer-4' ) ){
					$active = $active+1;
				}
				
				if($active>0){
					$column = 12/$active; 
				}
		   ?>
		   <?php if($active>0){ ?>
		   <?php if($moving_custom!=NULL and $moving_custom['footer-widgets']=='yes'){ ?> 
				<div class="up-footer">
				<div class="container">
					<div class="row">

						<div class="col-md-<?php echo esc_attr($column); ?>">
							<?php 
								
							if(is_active_sidebar('footer-1')){
								dynamic_sidebar('footer-1'); 
							}
							
							?>
						</div>

						<div class="col-md-<?php echo esc_attr($column); ?>">
							<?php 
								
							if(is_active_sidebar('footer-2')){
								dynamic_sidebar('footer-2'); 
							}
							
							?>

						</div>

						<div class="col-md-<?php echo esc_attr($column); ?>">
						<?php 
								
							if(is_active_sidebar('footer-3')){
								dynamic_sidebar('footer-3'); 
							}
							
							?>

						</div>

						<div class="col-md-<?php echo esc_attr($column); ?>">
							<?php 
								
							if(is_active_sidebar('footer-4')){
								dynamic_sidebar('footer-4'); 
							}
							
							?>

						</div>

					</div>
					</div>
				</div>
				<?php } } ?>
			<div class="container">
				<p class="copyright-line"><?php echo wp_kses_post($moving_custom['footer-text']); ?></p>

			</div>
		</footer>
		<!-- End footer -->

	</div>
	<!-- End Container -->
	<?php wp_footer(); ?>

</body>

</html>