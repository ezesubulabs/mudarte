mainApp.config(function ($stateProvider) {
    $stateProvider
        .state('miniBodegas', {
            abstract: true,
            templateUrl: '/wp-content/themes/moving/angular/views/miniBodegas/form.html',
            controller: 'miniBodegasController',
            controllerAs: '$ctrl'
        })
        .state('miniBodegas.first_step', {
            url: 'miniBodegas/first_step',
            templateUrl: '/wp-content/themes/moving/angular/views/miniBodegas/first_step.html'
        })
        .state('miniBodegas.second_step', {
            url: 'miniBodegas/second_step',
            templateUrl: '/wp-content/themes/moving/angular/views/miniBodegas/second_step.html'
        })
        .state('miniBodegas.third_step', {
            url: 'miniBodegas/third_step',
            templateUrl: '/wp-content/themes/moving/angular/views/miniBodegas/third_step.html'
        })
        .state('miniBodegas.four_step', {
            url: 'miniBodegas/four_step',
            templateUrl: '/wp-content/themes/moving/angular/views/miniBodegas/four_step.html'
        })
        .state('miniBodegas.five_step', {
            url: 'miniBodegas/five_step',
            templateUrl: '/wp-content/themes/moving/angular/views/miniBodegas/five_step.html'
        });
});
// Controller
mainApp.controller('miniBodegasController', function ($rootScope, $scope, $state, NgMap, paymentsService) {
    // we will store all of our form data in this object
    var $ctrl = this;
    $ctrl.formData = {};
    $ctrl.formData.mounths = 1;
    $ctrl.formData.addons = [
        {
            qty: 0,
            name: 'addon1',
            description: 'Costo de m2 extra',
            cost: 240,
            min:0,
            max:20
        },{
            qty: 0,
            name: 'addon2',
            description: 'Costo de carga y descarga (Estos costos solo aplican en patio)',
            cost: 840,
            min:0,
            max:20

        },{
            qty: 0,
            name: 'addon3',
            description: 'Cajas de candado para extra seguridad',
            cost: 0,
            min:0,
            max:1
        }
    ];


    $ctrl.packages = [
        {
            id: 1,
            title: "RESIDENCIAL 7M³",
            description: "¡Justo la medida que necesitas!",
            items: ['Carga y descarga','Empaque especial para almacenar', 'Vigilancia las 24 horas', 'Selladas en contra del medio ambiente', 'Acceso controlado', 'Cámaras de seguridad', 'Cajas de candado para extra seguridad'],
            precio: 1550,
            image: "cot-residencial1.png",
            horas:1
        },
        {
            id: 2,
            title: "RESIDENCIAL 14M³",
            description: "¡Justo la medida que necesitas!",
            items: ['Carga y descarga','Empaque especial para almacenar', 'Vigilancia las 24 horas', 'Selladas en contra del medio ambiente', 'Acceso controlado', 'Cámaras de seguridad', 'Cajas de candado para extra seguridad'],
            precio: 3000,
            image: "cot-residencial2.png",
            horas:1
        }];
    // functions
    $scope.positions = new google.maps.LatLng(25.6490373, -100.4435235);
    // $scope.origin = new google.maps.LatLng(25.6490373, -100.4435235);

    $scope.fnPayment = function(){
        successResponseHandler = function(token) {
            $ctrl.formData.token_id= token.id;
            var promise = paymentsService.create($ctrl.formData);
            promise.then(function (objData) {
                console.log("el response", objData)
                alert(objData.message);
                if(objData.type == 'success')
                    window.location.replace('/home/#/');
            });
        };
        errorResponseHandler = function(response) {
            alert(response.message_to_purchaser);
        };
        Conekta.Token.create($ctrl.formData, successResponseHandler, errorResponseHandler);
    };

    $scope.range = function(max, min){
        var ratings = [];
        for (var i = min; i <= max; i++) {
            ratings.push(i)
        }
        return ratings;
    };
    $scope.autocompleteOptions = {
        componentRestrictions: {country: 'MX'},
        radius: 50,
        location: $scope.positions,
        types: ['address']
    };

    NgMap.getMap().then(function (map) {
        $ctrl.map = map;
    });

    $scope.fnSelectPackage = function (obj) {
        $ctrl.formData.package = obj;
        $state.go('miniBodegas.third_step');
    };

    $scope.total = function() {
        var total = 0;
        if($ctrl.formData.package)
            total = $ctrl.formData.package.precio * $ctrl.formData.package.horas;
        angular.forEach($ctrl.formData.addons, function(item) {
            if(item.qty)
            total += item.qty * item.cost;
        });
        $ctrl.formData.total = total;
        return total;
    }

    $scope.today = function () {
        $scope.dt = new Date();
    };
    $scope.today();

    $scope.clear = function () {
        $scope.dt = null;
    };

    $scope.inlineOptions = {
        customClass: getDayClass,
        minDate: new Date(),
        showWeeks: true
    };

    $scope.dateOptions = {
        dateDisabled: disabled,
        formatYear: 'yy',
        maxDate: new Date(2020, 5, 22),
        minDate: new Date(),
        startingDay: 1
    };

    // Disable weekend selection
    function disabled(data) {
        var date = data.date,
            mode = data.mode;
        return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
    }

    $scope.toggleMin = function () {
        $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
        $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
    };

    $scope.toggleMin();

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.setDate = function (year, month, day) {
        $scope.dt = new Date(year, month, day);
    };


    $scope.popup1 = {
        opened: false
    };

    $scope.popup2 = {
        opened: false
    };

    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    var afterTomorrow = new Date();
    afterTomorrow.setDate(tomorrow.getDate() + 1);
    $scope.events = [
        {
            date: tomorrow,
            status: 'full'
        },
        {
            date: afterTomorrow,
            status: 'partially'
        }
    ];

    function getDayClass(data) {
        var date = data.date,
            mode = data.mode;
        if (mode === 'day') {
            var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

            for (var i = 0; i < $scope.events.length; i++) {
                var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                if (dayToCheck === currentDay) {
                    return $scope.events[i].status;
                }
            }
        }

        return '';
    }
});

