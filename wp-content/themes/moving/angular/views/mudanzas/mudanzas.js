mainApp.config(function ($stateProvider) {
    $stateProvider
        .state('mudanza', {
            abstract: true,
            templateUrl: '/wp-content/themes/moving/angular/views/mudanzas/form.html',
            controller: 'mudanzaController',
            controllerAs: '$ctrl'
        })
        .state('mudanza.first_step', {
            url: 'mudanzas/first_step',
            templateUrl: '/wp-content/themes/moving/angular/views/mudanzas/first_step.html'
        })
        .state('mudanza.second_step', {
            url: 'mudanzas/second_step',
            templateUrl: '/wp-content/themes/moving/angular/views/mudanzas/second_step.html'
        })
        .state('mudanza.third_step', {
            url: 'mudanzas/third_step',
            templateUrl: '/wp-content/themes/moving/angular/views/mudanzas/third_step.html'
        })
        .state('mudanza.four_step', {
            url: 'mudanzas/four_step',
            templateUrl: '/wp-content/themes/moving/angular/views/mudanzas/four_step.html'
        })
        .state('mudanza.five_step', {
            url: 'mudanzas/five_step',
            templateUrl: '/wp-content/themes/moving/angular/views/mudanzas/five_step.html'
        });
});

// Controller
mainApp.controller('mudanzaController', function ($rootScope, $scope, $location, $state,moment, NgMap, rentasService, paymentsService) {
    // we will store all of our form data in this object
    var $ctrl = this;

    $ctrl.formData = {};
    $ctrl.formData.addons = [{
        name: 'addon1',
        description: 'Hora adicional',
        cost: 225,
        min:5 ,
        max:7
    }, {

        name: 'addon2',
        description: 'Persona adicional',
        cost: 225,
        min:0 ,
        max:8
    },{

        name: 'addon3',
        description: 'Rollo de emplaye',
        cost: 200,
        min:0 ,
        max:20
    },{

        name: 'addon4',
        description: 'Rollo de Burbuja',
        cost: 450,
        min:0 ,
        max:20
    },{

        name: 'addon5',
        description: 'Rollo de papel corrugado',
        cost: 450,
        min:0 ,
        max:20
    },{

        name: 'addon6',
        description: 'Cinta canela',
        cost: 35,
        min:0 ,
        max:20
    },{

        name: 'addon7',
        description: 'Renta adicionales de cajas de plastico',
        description2:'*por cada 6 cajas por dos dias',
        cost: 250,
        min:0 ,
        max:20
    }
    ];


    $ctrl.packages = [
        {
            id: 1,
            title: "MÚDATE FÁCIL",
            description: "Camioneta o pick-up y/o camion caja cerrada 20m cubicos",
            items: ["2 personas para el servicio", "Proteccion con colchotenas anti impacto", "Translado a su nuevo domicilio", "Maniobras de carga, descarga del menaje y acomodo "],
            precio: 650,
            image: "cot-mudate-facil.png",
            horas: 1

        },
        {
            id: 2,
            title: "MÚDATE TÚ",
            description: "Pick-up y/o remolque o camion caja cerrada 20m cubicos",
            items: ["3 personas para el servicio", "Proteccion con colchotenas anti impacto", "Préstamo de 10 cajas de plástico para guardar articulos personales", "Prestamo de 2 cajones de madera para colgar"],
            precio: 800,
            image: "cot-mudate-tu.png",
            horas: 2
        },
        {
            id: 3,
            title: "MÚDATE",
            description: "Camión con caja cerrada de 3.5 toneladas 27 metros cubicos",
            items: ["4 personas para el servicio", "Proteccion con colchotenas anti impacto y un rollo de empaque", "Préstamo de 12 cajas de plástico", "Prestamo de 3 cajones de madera para colgar", "Incluye 7 horas de servicio"],
            precio: 1250,
            image: "cot-mudate.png",
            horas: 3,
            horas_descripcion: ""
        },
        {
            id: 4,
            title: "MÚDANZA",
            description: "Camión con caja cerrada de 3.5 toneladas 27 metros cubicos",
            items: ["4 personas para el servicio", "Proteccion con colchotenas anti impacto y un rollo de empaque", "Préstamo de 12 cajas de plástico", "Prestamo de 3 cajones de madera para colgar", "Incluye 7 horas de servicio"],
            precio: 7500,
            image: "cot-mudanza.png",
            horas: 1
        }
    ];
    // functions
    $scope.positions = new google.maps.LatLng(25.6490373, -100.4435235);
    // $scope.origin = new google.maps.LatLng(25.6490373, -100.4435235);

    $scope.autocompleteOptions = {
        componentRestrictions: {country: 'MX'},
        radius: 50,
        location: $scope.positions,
        types: ['address']
    };

    $scope.fnPayment = function(){
        successResponseHandler = function(token) {
            $ctrl.formData.token_id= token.id;
            var promise = paymentsService.create($ctrl.formData);
            promise.then(function (objData) {
                console.log("el response", objData)
                alert(objData.message);
                if(objData.type == 'success')
                    window.location.replace('/home/#/');
            });
        };
        errorResponseHandler = function(response) {
            alert(response.message_to_purchaser);
        };
        Conekta.Token.create($ctrl.formData, successResponseHandler, errorResponseHandler);

    };

    $scope.checkDisponibilidad = function(){
        $ctrl.formData.service_id = 1;
        var promise = rentasService.existService($ctrl.formData);
        promise.then(function (objData) {
            console.log("pero keee ", objData);
         if(objData.message == 0){
             $state.go('mudanza.four_step');
         }
         else{
             alert("No se encuetran vehiculos disponibles para esa hora, por favor seleccione otra");
         }
        });
        
    };

    NgMap.getMap().then(function (map) {
        $ctrl.map = map;
    });

    $scope.fnSelectPackage = function (obj) {
        $ctrl.formData.package = obj;
        $state.go('mudanza.third_step');
    };


    $scope.range = function(max, min){
        var ratings = [];
        for (var i = min; i <= max; i++) {
            ratings.push(i)
        }
        return ratings;
    };
    $scope.total = function() {
        if($ctrl.formData.package){
            var total = $ctrl.formData.package.precio * $ctrl.formData.package.horas;

            if(moment($ctrl.formData.hour, "HH:mm") > moment("18:01", "HH:mm") &&  moment($ctrl.formData.hour, "HH:mm") < moment("22:00", "HH:mm")){
                total += total * 0.25;
            }
            if(moment($ctrl.formData.hour, "HH:mm") > moment("22:00", "HH:mm") ){
                total += total * 0.30;
            }
            if(moment($ctrl.formData.hour, "HH:mm") < moment("08:00", "HH:mm") ){
                total += total * 0.30;
            }

        }
        angular.forEach($ctrl.formData.addons, function(item) {
            if(item.qty)
                total += item.qty * item.cost;

        });
        $ctrl.formData.total = total;
        return total;
    }

    $scope.today = function () {
        $scope.dt = new Date();
    };
    $scope.today();

    $scope.clear = function () {
        $scope.dt = null;
    };

    $scope.inlineOptions = {
        customClass: getDayClass,
        minDate: new Date(),
        showWeeks: true
    };

    $scope.dateOptions = {
        //dateDisabled: disabled,
        formatYear: 'yy',
        maxDate: new Date(2020, 5, 22),
        minDate: new Date(),
        startingDay: 1
    };

    // Disable weekend selection
    // function disabled(data) {
    //     var date = data.date,
    //         mode = data.mode;
    //     return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
    // }

    $scope.toggleMin = function () {
        $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
        $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
    };

    $scope.toggleMin();

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.setDate = function (year, month, day) {
        $scope.dt = new Date(year, month, day);
    };


    $scope.popup1 = {
        opened: false
    };

    $scope.popup2 = {
        opened: false
    };

    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    var afterTomorrow = new Date();
    afterTomorrow.setDate(tomorrow.getDate() + 1);
    $scope.events = [
        {
            date: tomorrow,
            status: 'full'
        },
        {
            date: afterTomorrow,
            status: 'partially'
        }
    ];

    function getDayClass(data) {
        var date = data.date,
            mode = data.mode;
        if (mode === 'day') {
            var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

            for (var i = 0; i < $scope.events.length; i++) {
                var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                if (dayToCheck === currentDay) {
                    return $scope.events[i].status;
                }
            }
        }

        return '';
    }
});

