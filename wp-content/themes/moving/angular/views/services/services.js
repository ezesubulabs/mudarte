/**
 * Home View
 */

// Route
mainApp.config( function( $stateProvider ) {
	$stateProvider.state( {
		name: 'services',
		url: '/services',
		templateUrl: '/wp-content/themes/moving/angular/views/services/services.html',
		controller: 'servicesCtrl',
        controllerAs: '$ctrl'
	} );
} );

// Controller
mainApp.controller( 'servicesCtrl', function( $scope, $state ) {
	//variables
    var $ctrl = this;
    //functions
    $ctrl.fnStarService = fnStarService;


    function fnStarService(){
        switch ($ctrl.service) {
			case 'carga':
                $state.go($state.go('movingHelpers.first_step'));
                break;
			case '2':
                $state.go($state.go('movingHelpers'));
                break;
            default:
            	alert("Debes seleccionar un caso");

        }
	}
} );