mainApp.config(function ($stateProvider) {
    $stateProvider
        .state('personal', {
            abstract: true,
            templateUrl: '/wp-content/themes/moving/angular/views/personal/form.html',
            controller: 'personalController',
            controllerAs: '$ctrl'
        })
        .state('personal.first_step', {
            url: 'personal/first_step',
            templateUrl: '/wp-content/themes/moving/angular/views/personal/first_step.html'
        })
        .state('personal.second_step', {
            url: 'personal/second_step',
            templateUrl: '/wp-content/themes/moving/angular/views/personal/second_step.html'
        });

});
// Controller
mainApp.controller('personalController', function ($rootScope, $scope, $state, NgMap,paymentsService) {
    // we will store all of our form data in this object
    var $ctrl = this;


    $ctrl.formData = {};
    // $ctrl.formData.card = {};
    // $ctrl.formData.objUser = {};
    // $ctrl.formData.objUser.firstName = "Ezequiel Suarez";
    // $ctrl.formData.objUser.lastName = "Buitrago";
    // $ctrl.formData.objUser.zipCode = "56562";
    // $ctrl.formData.objUser.direction = "Court Street";
    // $ctrl.formData.objUser.exteriorNumber = "16c";
    // $ctrl.formData.objUser.interiorNumber = "16c";
    // $ctrl.formData.objUser.extraDirection = "16c";
    // $ctrl.formData.objUser.colony = "Utoopua";
    // $ctrl.formData.objUser.delegation = "Fairfield";
    // $ctrl.formData.objUser.phone = "6507468586";
    // $ctrl.formData.objUser.email = "ezesubu_@gmail.com";
    // $ctrl.formData.card.name = "eze";
    // $ctrl.formData.card.exp_month = "12";
    // $ctrl.formData.card.cvc = "124";
    // $ctrl.formData.card.exp_year = "2020";
    // $ctrl.formData.card.number = '4242424242424242';

    $ctrl.formData.addons = [{
        name: 'addon1',
        description: 'Hora adicional',
        cost: 225,
        min:5 ,
        max:7
    }, {
        name: 'addon2',
        description: 'Persona adicional',
        cost: 225,
        min:2,
        max:8
    },{
        name: 'addon3',
        description: 'Rollo de emplaye',
        cost: 200,
        min:0 ,
        max:20
    },{

        name: 'addon4',
        description: 'Rollo de Burbuja',
        cost: 450,
        min:0 ,
        max:20
    },{

        name: 'addon5',
        description: 'Rollo de papel corrugado',
        cost: 450,
        min:0 ,
        max:20
    },{
        name: 'addon6',
        description: 'Cinta canela',
        cost: 35,
        min:0 ,
        max:20
    },{

        name: 'addon7',
        description: 'Renta adicionales de cajas de plastico',
        description2:'*por cada 6 cajas por dos dias',
        cost: 250,
        min:0,
        max:20
    }];

    $ctrl.formData.package =
        {
            id: 5,
            title: "TU PERSONAL ESPECIALIZADO",
            items: ["2 personas para el servicio", "Proteccion con colchotenas anti impacto", "Translado a su nuevo domicilio", "Maniobras de carga, descarga del menaje y acomodo "],
            precio: 650,
            horas: 1,
            image: "cot-personal.png",
            description: "Mudanza tipo personal especializado"

        };

    // functions
    $scope.positions = new google.maps.LatLng(25.6490373, -100.4435235);
    // $scope.origin = new google.maps.LatLng(25.6490373, -100.4435235);

    $scope.autocompleteOptions = {
        componentRestrictions: {country: 'MX'},
        radius: 50,
        location: $scope.positions,
        types: ['address']
    };

    $scope.fnPayment = function(){
        successResponseHandler = function(token) {
            $ctrl.formData.token_id= token.id;
            var promise = paymentsService.create($ctrl.formData);
            promise.then(function (objData) {
                console.log("el response", objData)
                alert(objData.message);
                if(objData.type == 'success')
                    window.location.replace('/home/#/');
            });
        };
        errorResponseHandler = function(response) {
            alert(response.message_to_purchaser);
        };
        Conekta.Token.create($ctrl.formData, successResponseHandler, errorResponseHandler);

    };

    $scope.range = function(max, min){
        var ratings = [];
        for (var i = min; i <= max; i++) {
            ratings.push(i)
        }
        return ratings;
    };

    NgMap.getMap().then(function (map) {
        $ctrl.map = map;
    });

    $scope.fnSelectPackage = function (obj) {
        $ctrl.formData.package = obj;
        $state.go('pers.third_step');
    };

    $scope.total = function() {
        var total = 0;
        if($ctrl.formData.package)
            total+=$ctrl.formData.package.precio;
        angular.forEach($ctrl.formData.addons, function(item) {
            if(item.qty)
                total += item.qty * item.cost;
        });
        $ctrl.formData.total = total;
        return total;
    }

    $scope.today = function () {
        $scope.dt = new Date();
    };
    $scope.today();

    $scope.clear = function () {
        $scope.dt = null;
    };

    $scope.inlineOptions = {
        customClass: getDayClass,
        minDate: new Date(),
        showWeeks: true
    };

    $scope.dateOptions = {
        dateDisabled: disabled,
        formatYear: 'yy',
        maxDate: new Date(2020, 5, 22),
        minDate: new Date(),
        startingDay: 1
    };

    // Disable weekend selection
    function disabled(data) {
        var date = data.date,
            mode = data.mode;
        return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
    }

    $scope.toggleMin = function () {
        $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
        $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
    };

    $scope.toggleMin();

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    $scope.setDate = function (year, month, day) {
        $scope.dt = new Date(year, month, day);
    };


    $scope.popup1 = {
        opened: false
    };

    $scope.popup2 = {
        opened: false
    };

    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    var afterTomorrow = new Date();
    afterTomorrow.setDate(tomorrow.getDate() + 1);
    $scope.events = [
        {
            date: tomorrow,
            status: 'full'
        },
        {
            date: afterTomorrow,
            status: 'partially'
        }
    ];

    function getDayClass(data) {
        var date = data.date,
            mode = data.mode;
        if (mode === 'day') {
            var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

            for (var i = 0; i < $scope.events.length; i++) {
                var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                if (dayToCheck === currentDay) {
                    return $scope.events[i].status;
                }
            }
        }

        return '';
    }
});

