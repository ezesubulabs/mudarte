/**
 * Angular.js App: mainApp
 */
// =require ../vendor/lodash.js
// =require ../vendor/angular.js
// =require ../vendor/angular-resource.js
// =require ../vendor/angular-sanitize.js
// =require ../vendor/angular-animate.js
// =require ../vendor/angular-ui-router.js
// =require ../node_modules/angular-ui-bootstrap/dist/ui-bootstrap.js
// =require ../node_modules/angular-ui-bootstrap/dist/ui-bootstrap-tpls.js
// =require ../node_modules/ngmap/build/scripts/ng-map.min.js
// =require ../node_modules/angular-google-places-autocomplete/src/autocomplete.js
// =require ../node_modules/restangular/src/restangular.js
// =require ../node_modules/moment/moment.js
// =require ../node_modules/angular-moment/angular-moment.js
// =require ../node_modules/angular-messages/angular-messages.js


// App module

var mainApp = angular.module( "mainApp", [
	'ng',
	'ngResource',
	'ngSanitize',
	'ui.router',
	'ngAnimate',
    'ui.bootstrap',
    'ngMap',
	'google.places',
	'restangular',
    'angularMoment',
	'ngMessages'
] )
// Third-party support
mainApp.constant( '_', window._ );
mainApp.constant( 'WP', window.WP );


// Boot
mainApp.run( function( $rootScope, _ ) {

	// Log routing errors
	$rootScope.$on( "$stateChangeError", console.error.bind( console, '$stateChangeError' ) );

	// Global lodash
	$rootScope._ = window._;
} );

// =require bootstrap.js
// =require views/mudanzas/mudanzas.js
// =require views/services/services.js
// =require views/personal/personal.js
// =require views/miniBodegas/miniBodegas.js
// =require services/rentasService.js
// =require services/paymentsService.js

