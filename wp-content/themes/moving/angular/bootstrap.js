/**
 * bootstrap redirect service
 */
// Route
mainApp.config( function( $stateProvider, RestangularProvider) {
    $stateProvider.state( {
        name: 'bootstrap',
        url: '/?type',
        controller: 'bootstrapCtrl',
        controllerAs: '$ctrl'
    } )
    //// Restangular
    //RestangularProvider.setDefaultHttpFields({withCredentials: false});
    //RestangularProvider.setFullResponse(true);
    RestangularProvider.setBaseUrl('https://adminmudanzas.redpill.mx/');
    Conekta.setPublicKey('key_OfChs1zrMjsPs1rc5yWLoHg');

} );

// Controller
mainApp.controller( 'bootstrapCtrl', function( $scope, $stateParams, $state  ) {
    //variables
    var $ctrl = this;
    var type = $stateParams.type;

    fnStarService(type);

    function fnStarService(type){
        switch (type) {
            case 'mudanzas':
                $state.go($state.go('mudanza.first_step'));
                break;
            case 'miniBodegas':
                $state.go($state.go('miniBodegas.first_step'));
                break;
            case 'personal':
                $state.go($state.go('personal.first_step'));
                break;
            case 'arte':
                window.location.href = '/obras-de-arte/';
                break;
            default:
                alert("Debes seleccionar un caso");

        }
    }

} );