// Controller
mainApp.service('rentasService', function ($q, Restangular) {

    // we will store all of our form data in this object
    var resource = Restangular.all('renta');
    //
     this.existService = existService;
    //
    function existService(formData) {
        var def = $q.defer();

        var promise = resource.customGET('getByDateTime', {date: formData.date, hour: formData.hour, service_id: formData.service_id });

        promise.then(
            function (objResponse) {
                def.resolve(angular.copy(objResponse));
            }, function (objResponse) {
                def.reject(objResponse);
            }
        );

        return def.promise;
    }
});