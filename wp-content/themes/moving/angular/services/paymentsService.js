// Controller
mainApp.service('paymentsService', function ($q, Restangular) {

    // we will store all of our form data in this object
    var resource = Restangular.all('payment');
    //
    this.create = create;
    //
    function create(formData) {
        var def = $q.defer();

            var promise = resource.post(formData);

        promise.then(
            function (objResponse) {
                def.resolve(angular.copy(objResponse));

            }, function (objResponse) {
                def.reject(objResponse.error);
            }
        );

        return def.promise;
    }
});