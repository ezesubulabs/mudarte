<?php get_header(); $moving_custom  = moving_custom();?>

        <!-- page-banner-section
            ================================================== -->
        <section class="page-banner-section blog">
            <div class="container">
                <div class="page-banner-box">
                    <h1><?php esc_html_e('latest new','moving'); ?>.</h1>
                    <?php if($moving_custom != null and  $moving_custom['quote']!=''){ ?>
                    <a href="<?php echo esc_url($moving_custom['quote']); ?>"><?php esc_html_e('Request a Free Quote','moving'); ?></a>
                    <?php } ?>
                </div>
            </div>
        </section>
        <!-- End page-banner section -->

        <!-- blog-section
            ================================================== -->
        <section class="blog-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <div class="blog-box">
                            <?php
                            if(have_posts()) :
                                            while(have_posts()) : the_post();
                            ?>
                            <div <?php post_class('blog-post');?>>
                                <div class="row">
                                <?php if(has_post_thumbnail()){ ?>
                                    <div class="col-sm-6">
                                        <?php the_post_thumbnail();?>
                                    </div>
                                    <div class="col-sm-6">
                                        <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                                        <ul class="post-meta">
                                            <li>
                                                <i class="fa fa-calendar"></i>
                                                <?php the_time(get_option( 'date_format' )); ?>
                                            </li>
                                            <li>
                                                <i class="fa fa-user"></i>
                                                <?php esc_html_e('by','moving'); ?> <?php the_author_posts_link(); ?>
                                            </li>
                                            <li>
                                                <i class="fa fa-comments"></i> <?php
                                                                              comments_popup_link( esc_html__('0 comments','moving'), esc_html__('1 comment','moving'), esc_html__('% comments','moving'));
                                                                            ?>
                                            </li>
                                        </ul>
                                        <p><?php echo moving_excerpt(); ?></p>
                                        <a class="button-continue" href="<?php the_permalink(); ?>"><?php esc_html_e('Continue Reading','moving'); ?></a>
                                    </div>
                                    <?php }else{ ?>
                                        <div class="col-sm-12">
                                        <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                                        <ul class="post-meta">
                                            <li>
                                                <i class="fa fa-calendar"></i>
                                                <?php the_time(get_option( 'date_format' )); ?>
                                            </li>
                                            <li>
                                                <i class="fa fa-user"></i>
                                                <?php esc_html_e('by','moving'); ?> <?php the_author_posts_link(); ?>
                                            </li>
                                            <li>
                                                <i class="fa fa-comments"></i> <?php
                                                                              comments_popup_link( esc_html__('0 comments','moving'), esc_html__('1 comment','moving'), esc_html__('% comments','moving'));
                                                                            ?>
                                            </li>
                                        </ul>
                                        <?php the_excerpt(); ?>
                                        <a class="button-continue" href="<?php the_permalink(); ?>"><?php esc_html_e('Continue Reading','moving'); ?></a>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                             <?php endwhile; ?>

                            <?php else: ?>
                            <div class="not-found">
                                    <h1><?php esc_html_e('Nothing Found Here!','moving'); ?></h1>
                                    <h3><?php esc_html_e('Search with other keyword:', 'moving') ?></h3>
                                    <div class="search-widget">
                                            <?php get_search_form(); ?>
                                    </div>
                            </div>
                        <?php endif; ?>
                            <div class="pagination-box">
                                <?php moving_pagination($prev = '', $next = '', $pages=''); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">

                        <?php get_sidebar(); ?>

                    </div>
                </div>
            </div>
        </section>
        <!-- End blog section -->


<?php get_footer(); ?>