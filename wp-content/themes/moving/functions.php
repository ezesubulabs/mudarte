<?php

require_once get_template_directory() . '/framework/import.php';
require_once get_template_directory() . '/framework/theme-configs.php';
require_once get_template_directory() . '/framework/widgets/recent.php';
require_once get_template_directory() . '/framework/widgets/recent_posts.php';
require_once get_template_directory() . '/framework/widgets/recent_testimonial.php';
require_once get_template_directory() . '/framework/moving_bootstrap_navwalker.php';
if(function_exists('vc_add_param')){
require_once get_template_directory() . '/vc_functions.php';
}

if ( ! isset( $content_width ) )
	$content_width = 604;

function moving_setup() {
	/*
	 * Makes moving available for translation.
	 *
	 * Translations can be added to the /languages/ directory.
	 * If you're building a theme based on moving, use a find and
	 * replace to change 'moving' to the name of your theme in all
	 * template files.
	 */
	load_theme_textdomain( 'moving', get_template_directory() . '/languages' );

	
	// Adds RSS feed links to <head> for posts and comments.
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'title-tag' );
	 add_theme_support( 'custom-header');
	 add_theme_support( 'custom-background');
	/*
	 * Switches default core markup for search form, comment form,
	 * and comments to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menu( 'primary', esc_html__( 'Main Navigation Menu', 'moving' ) );

	/*
	 * This theme uses a custom image size for featured images, displayed on
	 * "standard" posts and pages.
	 */
	add_theme_support( 'post-thumbnails' );
	
	
}
add_action( 'after_setup_theme', 'moving_setup' );

function moving_thumbnail_url($size, $id){
    global $post;
    if($id==''){
        $id = $post->ID;
    }
    if($size==''){
        $url = wp_get_attachment_url( get_post_thumbnail_id($id) );
         return $url;
    }else{
        $url = wp_get_attachment_image_src( get_post_thumbnail_id($id), $size);
         return $url[0];
    }
   
}

function moving_scripts_styles() {
	global $moving_options;
	/*
	 * Adds JavaScript to pages with the comment form to support
	 * sites with threaded comments (when in use).
	 */
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	
	// Loads JavaScript file with functionality specific to moving.

	wp_enqueue_script("jquery-ui", get_template_directory_uri()."/js/jquery-ui.min.js",array('jquery'),'1.12.1',true);
	wp_enqueue_script("bootstrap", get_template_directory_uri()."/js/bootstrap.min.js",array('jquery'),'3.3.1',true);
	wp_enqueue_script("flexslider", get_template_directory_uri()."/js/jquery.flexslider.js",array('jquery'),'2.0',true);
	wp_enqueue_script("appear", get_template_directory_uri()."/js/jquery.appear.js",array('jquery'),false,true);
	wp_enqueue_script("countTo", get_template_directory_uri()."/js/jquery.countTo.js",array('jquery'),false,true);
	wp_enqueue_script("owl.carousel", get_template_directory_uri()."/js/owl.carousel.min.js",array('jquery'),'1.3.3',true);
	wp_enqueue_script("moving-script", get_template_directory_uri()."/js/script.js",array('jquery'),'1.0',true);
    wp_enqueue_script("angular-data", get_template_directory_uri()."/dist/js/main.js",array(),'0.1',false);
    wp_enqueue_script('masonry');
	
	
	// Add Source Sans Pro and Bitter fonts, used in the main stylesheet.
	
	wp_enqueue_style( 'bootstrap', get_template_directory_uri().'/css/bootstrap.min.css','3.3.1');
	wp_enqueue_style( 'font-awesome', get_template_directory_uri().'/css/font-awesome.min.css','4.7.0');
	wp_enqueue_style( 'elegant-icons', get_template_directory_uri().'/css/elegant-icons.css');
	wp_enqueue_style( 'linearicons', get_template_directory_uri().'/css/linearicons.css');
	wp_enqueue_style( 'owl.carousel', get_template_directory_uri().'/css/owl.carousel.css','1.24');
	wp_enqueue_style( 'owl.theme', get_template_directory_uri().'/css/owl.theme.css','1.24');
	wp_enqueue_style( 'jquery-ui', get_template_directory_uri().'/css/jquery-ui.min.css','1.12.1');
	wp_enqueue_style( 'flexslider', get_template_directory_uri().'/css/flexslider.css','2.0');
	wp_enqueue_style( 'moving-default', get_template_directory_uri().'/css/moving_style.css','1.0');
    wp_enqueue_style( 'angularStyle', get_template_directory_uri().'/dist/css/main.css','1.0');
	// Loads our main stylesheet.
	wp_enqueue_style( 'moving-style', get_stylesheet_uri(), array(), '2017-08-02' );
	
	

}
add_action( 'wp_enqueue_scripts', 'moving_scripts_styles' );

/*
Register Fonts
*/
function moving_fonts_url() {
	global $moving_options;
    $font_url = '';
    
    /*
    Translators: If there are characters in your language that are not supported
    by chosen font(s), translate this to 'off'. Do not translate into your own language.
     */
    if ( 'off' !== _x( 'on', 'Google font: on or off', 'moving' ) ) {
        $font_url = add_query_arg( 'family', urlencode( $moving_options['body-font2']['font-family'].':400,500,700,500italic,400italic,300italic,300,100,100italic,700italic,900,900italic&subset=latin,latin-ext' ), "//fonts.googleapis.com/css" );
    }
    return $font_url;
}
/*
Enqueue scripts and styles.
*/
function moving_scripts() {
	global $moving_options;
	if($moving_options!=null && $moving_options['body-font2']['font-family'] != ''){
		wp_enqueue_style( 'moving-fonts', moving_fonts_url(), array(), '1.0.0' );
	}
}
add_action( 'wp_enqueue_scripts', 'moving_scripts' );



//Colors


function moving_hex2rgb($hex) {
   $hex = str_replace("#", "", $hex);

   if(strlen($hex) == 3) {
      $r = hexdec(substr($hex,0,1).substr($hex,0,1));
      $g = hexdec(substr($hex,1,1).substr($hex,1,1));
      $b = hexdec(substr($hex,2,1).substr($hex,2,1));
   } else {
      $r = hexdec(substr($hex,0,2));
      $g = hexdec(substr($hex,2,2));
      $b = hexdec(substr($hex,4,2));
   }
   $rgb = array($r, $g, $b);
  
   return $rgb; // returns an array with the rgb values
}


function moving_color() {
	wp_enqueue_style(
        'moving-custom-style',
        get_template_directory_uri() . '/css/custom_script.css'
    );
	global $moving_options, $wp_query;
	
	
	$moving_custom_css = "".$moving_options['custom-css'];
		
		$moving_custom_css .= "";
		
		
	

	wp_add_inline_style( 'moving-custom-style', $moving_custom_css );

}
add_action( 'wp_enqueue_scripts', 'moving_color' );



add_filter('wp_list_categories', 'moving__span_cat_count');
function moving__span_cat_count($links) {
$links = str_replace('</a> (', '</a> <span>(', $links);
$links = str_replace(')', ')</span>', $links);
return $links;
}


function moving_custom(){
	global $moving_options;

	$theme_options = $moving_options;

	return $theme_options;
}


function moving_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Main Sidebar Widget Area', 'moving' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Appears in sidebar.', 'moving' ),
		'before_widget' => '<aside id="%1$s" class="widget bottom_90 %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>',
	) );register_sidebar( array(
		'name'          => esc_html__( 'Footer Widget Area 1', 'moving' ),
		'id'            => 'footer-1',
		'description'   => esc_html__( 'Appears in footer.', 'moving' ),
		'before_widget' => '<aside id="%1$s" class="footer-widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	) );;register_sidebar( array(
		'name'          => esc_html__( 'Footer Widget Area 2', 'moving' ),
		'id'            => 'footer-2',
		'description'   => esc_html__( 'Appears in footer.', 'moving' ),
		'before_widget' => '<aside id="%1$s" class="footer-widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	) );;register_sidebar( array(
		'name'          => esc_html__( 'Footer Widget Area 3', 'moving' ),
		'id'            => 'footer-3',
		'description'   => esc_html__( 'Appears in footer.', 'moving' ),
		'before_widget' => '<aside id="%1$s" class="footer-widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	) );;register_sidebar( array(
		'name'          => esc_html__( 'Footer Widget Area 4', 'moving' ),
		'id'            => 'footer-4',
		'description'   => esc_html__( 'Appears in footer.', 'moving' ),
		'before_widget' => '<aside id="%1$s" class="footer-widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	) );
	
    
}
add_action( 'widgets_init', 'moving_widgets_init' );


function moving_excerpt($limit = 20) {
 
  $excerpt = explode(' ', get_the_excerpt(), $limit);
  if (count($excerpt)>=$limit) {
    array_pop($excerpt);
    $excerpt = implode(" ",$excerpt).'...';
  } else {
    $excerpt = implode(" ",$excerpt);
  }
  $excerpt = preg_replace('`[[^]]*]`','',$excerpt);
  return $excerpt;
}



function moving_excerpt_more( $more ) {
	return '...';
}
add_filter( 'excerpt_more', 'moving_excerpt_more' );


if (!function_exists('moving_pagination')) {

//pagination
function moving_pagination($prev = '', $next = '', $pages='') {
    global $wp_query, $wp_rewrite;
    $wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;
    if($pages==''){
        global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
    }if(is_front_page() and !is_home()) {
		$curent = (get_query_var('page')) ? get_query_var('page') : 1;
	} else {
		$curent = (get_query_var('paged')) ? get_query_var('paged') : 1;
	}
    $pagination = array(
			'base' 			=> str_replace( 999999999, '%#%', get_pagenum_link( 999999999 ) ),
			'format' 		=> '',
			'current' 		=> max( 1, $curent),
			'total' 		=> $pages,
			'prev_text' => $prev,
			'next_text' => $next,
			'type'			=> 'list',
			'end_size'		=> 2,
			'mid_size'		=> 1
	);
    $return =  paginate_links( $pagination );
	echo str_replace( "<ul class='page-numbers'>", '<ul class="page-list">', $return );
	?>
	<span class="prev"><?php previous_posts_link(esc_html__('&lt; prev','moving')); ?></span>
	<span class="next"><?php next_posts_link(esc_html__('next &gt;','moving'), $pages); ?></span>
	
<?php 
}
}

if (!function_exists('moving_breadcrumb')) {
function moving_breadcrumb() {

		$text['services']     = esc_html__('Home','moving'); // text for the 'Home' link
    $text['category'] = esc_html__('Archive by Category','moving').' "%s"'; // text for a category page
    $text['tax']       = esc_html__('Archive for','moving').' "%s"'; // text for a taxonomy page
    $text['search']   = esc_html__('Search Results for','moving').' "%s"'.esc_html(' Query','moving'); // text for a search results page
    $text['tag']      = esc_html__('Posts Tagged','moving').' "%s"'; // text for a tag page
    $text['author']   = esc_html__('Articles Posted','moving').' by %s'; // text for an author page
    $text['404']      = esc_html__('Error 404','moving'); // text for the 404 page
    $showCurrent = 1; // 1 - show current post/page title in breadcrumbs, 0 - don't show
    $showOnHome  = 0; // 1 - show breadcrumbs on the homepage, 0 - don't show
    $delimiter   = '<i class="fa fa-angle-double-right"></i>'; // delimiter between crumbs
    $before      = '<span class="curren">'; // tag before the current crumb
    $after       = '</span>'; // tag after the current crumb

    /* === END OF OPTIONS === */

    global $post;
    $homeLink = home_url('/');
    $linkBefore = '<li>';
    $linkAfter = '</li>';
    $linkAttr = ' rel="v:url" property="v:title"';
    $link = $linkBefore . '<a' . $linkAttr . ' href="%1$s">%2$s</a>' . $linkAfter;
    if (is_home() || is_front_page()) {
        if ($showOnHome == 1) echo '<div id="crumbs"><a href="' . esc_url($homeLink) . '">' . $text['services'] . '</a></div>';
    } else {
         echo '<ul id="breadcrumbs" class="page-depth">' . sprintf($link, $homeLink, $text['services']) . $delimiter;
        if ( is_category() ) {
            $thisCat = get_category(get_query_var('cat'), false);
            if ($thisCat->parent != 0) {
                $cats = get_category_parents($thisCat->parent, TRUE, $delimiter);
                $cats = str_replace('<a', $linkBefore . '<a' . $linkAttr, $cats);
                $cats = str_replace('</a>', '</a>' . $linkAfter, $cats);
                echo $cats;
            }
            echo $before . sprintf($text['category'], single_cat_title('', false)) . $after;
        } elseif( is_tax() ){
            $thisCat = get_category(get_query_var('cat'), false);
            if (isset($thisCat->parent)) {
                $cats = get_category_parents($thisCat->parent, TRUE, $delimiter);
                $cats = str_replace('<a', $linkBefore . '<a' . $linkAttr, $cats);
                $cats = str_replace('</a>', '</a>' . $linkAfter, $cats);
                echo $cats;
            }

            echo $before . sprintf($text['tax'], single_cat_title('', false)) . $after;
        }elseif ( is_search() ) {
            echo $before . sprintf($text['search'], get_search_query()) . $after;
        } elseif ( is_day() ) {
            echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $delimiter;
            echo sprintf($link, get_month_link(get_the_time('Y'),get_the_time('m')), get_the_time('F')) . $delimiter;
            echo $before . get_the_time('d') . $after;
        } elseif ( is_month() ) {
            echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $delimiter;
            echo $before . get_the_time('F') . $after;
        } elseif ( is_year() ) {
            echo $before . get_the_time('Y') . $after;
        } elseif ( is_single() && !is_attachment() ) {
            if ( get_post_type() != 'post' ) {
                $post_type = get_post_type_object(get_post_type());
                $slug = $post_type->rewrite;
                printf($link, $homeLink . '/' . $slug['slug'] . '/', $post_type->labels->singular_name);
                if ($showCurrent == 1) echo $delimiter . $before . get_the_title() . $after;
            } else {
                $cat = get_the_category(); $cat = $cat[0];
                $cats = get_category_parents($cat, TRUE, $delimiter);
                if ($showCurrent == 0) $cats = preg_replace("#^(.+)$delimiter$#", "$1", $cats);
                $cats = str_replace('<a', $linkBefore . '<a' . $linkAttr, $cats);
                $cats = str_replace('</a>', '</a>' . $linkAfter, $cats);
                echo $cats;
                if ($showCurrent == 1) echo $before . get_the_title() . $after;
            }
        } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
            $post_type = get_post_type_object(get_post_type());
            echo $before . $post_type->labels->singular_name . $after;
        } elseif ( is_attachment() ) {
            $parent = get_post($post->post_parent);
            $cat = get_the_category($parent->ID); $cat = $cat[0];
            $cats = get_category_parents($cat, TRUE, $delimiter);
            $cats = str_replace('<a', $linkBefore . '<a' . $linkAttr, $cats);
            $cats = str_replace('</a>', '</a>' . $linkAfter, $cats);
            echo $cats;
            printf($link, get_permalink($parent), $parent->post_title);
            if ($showCurrent == 1) echo $delimiter . $before . get_the_title() . $after;
        } elseif ( is_page() && !$post->post_parent ) {
            if ($showCurrent == 1) echo $before . get_the_title() . $after;
        } elseif ( is_page() && $post->post_parent ) {
            $parent_id  = $post->post_parent;
            $breadcrumbs = array();
            while ($parent_id) {
                $page = get_page($parent_id);
                $breadcrumbs[] = sprintf($link, get_permalink($page->ID), get_the_title($page->ID));
                $parent_id  = $page->post_parent;
            }
            $breadcrumbs = array_reverse($breadcrumbs);
            for ($i = 0; $i < count($breadcrumbs); $i++) {
                echo $breadcrumbs[$i];
                if ($i != count($breadcrumbs)-1) echo $delimiter;
            }
            if ($showCurrent == 1) echo $delimiter . $before . get_the_title() . $after;
        } elseif ( is_tag() ) {
            echo $before . sprintf($text['tag'], single_tag_title('', false)) . $after;
        } elseif ( is_author() ) {
             global $author;
            $userdata = get_userdata($author);
            echo $before . sprintf($text['author'], $userdata->display_name) . $after;
        } elseif ( is_404() ) {
            echo $before . $text['404'] . $after;
        }
        if ( get_query_var('paged') ) {
            if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
            echo esc_html__('Page','moving') . ' ' . get_query_var('paged');
            if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
        };
        echo '</ul>';
    }
}

}

//Custom comment List:
function moving_theme_comment($comment, $args, $depth) {
	
     $GLOBALS['comment'] = $comment; ?>
     <!--=======  COMMENTS =========-->
     
		<li <?php comment_class(''); ?> id="comment-<?php comment_ID() ?>" >

			<div class="comment-box">
                <?php if($comment->user_id!='0' and get_user_meta($comment->user_id, '_moving_avatar' ,true)!=''){ ?>
					<?php $image = get_user_meta($comment->user_id, '_moving_avatar' ,true); ?>
						<img src="<?php echo esc_attr($image); ?>" />
					<?php }else{ ?>
						<?php echo get_avatar($comment); ?>
				<?php } ?>
                <div class="comment-content">
                    <h4><?php printf(esc_html__('%s','moving'), get_comment_author_link()) ?></h4>
                    <span><?php printf(esc_html__('%1$s at %2$s','moving'), get_comment_date(), get_comment_time()) ?></span>
                    <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
                    <?php comment_text() ?>
					<?php if ($comment->comment_approved == '0') : ?>
						 <em><?php esc_html_e('Your comment is awaiting moderation.','moving') ?></em>
						 <br />
					 <?php endif; ?>
                </div>
            </div>

<?php

}

add_action( 'cmb2_admin_init', 'moving_metaboxes' );
/**
 * Define the metabox and field configurations.
 */
function moving_metaboxes() {
	global $moving_options; 
    // Start with an underscore to hide fields from custom fields list
    $prefix = '_moving_';

    /**
     * Initiate the metabox
     */
    $cmb = new_cmb2_box( array(
        'id'            => 'page_options',
        'title'         => esc_html__( 'Page Setting', 'moving' ),
        'object_types'  => array( 'page' ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
        
    ) );
   
   	 $cmb->add_field( array(
		'name'             => esc_html__( 'Turn on/off Breadcrumb section', 'moving' ),
		'desc'             => '',
		'id'               => $prefix . 'breadcrumb',
		'type'             => 'select',
		
		'options'          => array(
			'on' => esc_html__( 'On', 'moving' ),
			'off' => esc_html__( 'Off', 'moving' ),
			
		),
	) );

   

   	$cmb = new_cmb2_box( array(
        'id'            => 'team_options',
        'title'         => esc_html__( 'Team Options', 'moving' ),
        'object_types'  => array( 'team', ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
        
    ) );
   	 $cmb->add_field(  array(
        'name' => esc_html__('Member job','moving'),
        'desc' => '',
        'id'   => $prefix . 'job',
        'type'    => 'text',
        
    )); $cmb->add_field(  array(
        'name' => esc_html__('Member Social Links','moving'),
        'desc' => '',
        'id'   => $prefix . 'social',
        'type'    => 'textarea',
        
    ));

    $cmb = new_cmb2_box( array(
        'id'            => 'testimonial_options',
        'title'         => esc_html__( 'Testimonial Options', 'moving' ),
        'object_types'  => array( 'testimonial', ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
        
    ) );
   	 $cmb->add_field(  array(
        'name' => esc_html__('Subtitle','moving'),
        'desc' => '',
        'id'   => $prefix . 'job',
        'type'    => 'text',
        
    ));

    
    $cmb = new_cmb2_box( array(
        'id'            => 'user_edit',
        'title'         => esc_html__( 'User Meta Profile', 'moving' ),
        'object_types'  => array( 'user', ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
        
    ) );
    $cmb->add_field(
    	array(
				'name'    => esc_html__( 'Avatar', 'moving' ),
				'desc'    => esc_html__( 'field description (optional)', 'moving' ),
				'id'      => $prefix . 'avatar',
				'type'    => 'file',
				'save_id' => true,
			)
    );
    
 	
}


/**
 * This file represents an example of the code that themes would use to register
 * the required plugins.
 *
 * It is expected that theme authors would copy and paste this code into their
 * functions.php file, and amend to suit.
 *
 * @see http://tgmpluginactivation.com/configuration/ for detailed documentation.
 *
 * @package    TGM-Plugin-Activation
 * @subpackage Example
 * @version    2.6.1 for parent theme moving for publication on ThemeForest
 * @author     Thomas Griffin, Gary Jones, Juliette Reinders Folmer
 * @copyright  Copyright (c) 2011, Thomas Griffin
 * @license    http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @link       https://github.com/TGMPA/TGM-Plugin-Activation
 */

require_once get_template_directory() . '/framework/class-tgm-plugin-activation.php';
add_action( 'tgmpa_register', 'moving_register_required_plugins' );
/**
 * Register the required plugins for this theme.
 *
 * In this example, we register two plugins - one included with the TGMPA library
 * and one from the .org repo.
 *
 * The variable passed to tgmpa_register_plugins() should be an array of plugin
 * arrays.
 *
 * This function is hooked into tgmpa_init, which is fired within the
 * TGM_Plugin_Activation class constructor.
 */
function moving_register_required_plugins() {
    /**
     * Array of plugin arrays. Required keys are name and slug.
     * If the source is NOT from the .org repo, then source is also required.
     */
    $plugins = array(
             // This is an example of how to include a plugin from a private repo in your theme.

		
        array(            
            'name'               => esc_html__( 'QK Register Post Type', 'moving' ), 
            'slug'               => 'qk-post_type', 
            'source'             => get_template_directory() . '/framework/plugins/qk-post_type.zip', 
            'required'           => true, 
        ), array(            
            'name'               => esc_html__('Revolution Slider','moving'), // The plugin name.
            'slug'               => 'revslider', // The plugin slug (typically the folder name).
            'source'             => get_template_directory() . '/framework/plugins/revslider.zip', // The plugin source.
            'required'           => true, // If false, the plugin is only 'recommended' instead of required.
        ),
        
        
        // This is an example of how to include a plugin from the WordPress Plugin Repository.
		
        array(
            'name'      => esc_html__( 'Redux Framework', 'moving' ),
            'slug'      => 'redux-framework',
            'required'  => true,
        ), array(
            'name'      => esc_html__( 'Elementor Page Builder', 'moving' ),
            'slug'      => 'elementor',
            'required'  => true,
        ),array(
            'name'      => esc_html__( 'CMB2', 'moving' ),
            'slug'      => 'cmb2',
            'required'  => true,
        ),array(
            'name'      => esc_html__( 'Contact Form 7', 'moving' ),
            'slug'      => 'contact-form-7',
            'required'  => true,
        ),array(
            'name'      => esc_html__( 'Redux Developer Mode Disabler', 'moving' ),
            'slug'      => 'redux-developer-mode-disabler',
            'required'  => false,
        ),array(
            'name'      => esc_html__( 'One Click Demo Import', 'moving' ),
            'slug'      => 'one-click-demo-import',
            'required'  => false,
        ),
    );

    /**
     * Array of configuration settings. Amend each line as needed.
     * If you want the default strings to be available under your own theme domain,
     * leave the strings uncommented.
     * Some of the strings are added into a sprintf, so see the comments at the
     * end of each line for what each argument will be.
     */
    $config = array(
        'default_path' => '',                      // Default absolute path to pre-packaged plugins.
        'menu'         => 'tgmpa-install-plugins', // Menu slug.
        'has_notices'  => true,                    // Show admin notices or not.
        'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
        'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
        'is_automatic' => false,                   // Automatically activate plugins after installation or not.
        'message'      => '',                      // Message to output right before the plugins table.
        'strings'      => array(
            'page_title'                      => esc_html__( 'Install Required Plugins', 'moving' ),
            'menu_title'                      => esc_html__( 'Install Plugins', 'moving' ),
            'installing'                      => esc_html__( 'Installing Plugin: %s', 'moving' ), // %s = plugin name.
            'oops'                            => esc_html__( 'Something went wrong with the plugin API.', 'moving' ),
            'notice_can_install_required'     => _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.', 'moving' ), // %1$s = plugin name(s).
            'notice_can_install_recommended'  => _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.', 'moving' ), // %1$s = plugin name(s).
            'notice_cannot_install'           => _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.', 'moving' ), // %1$s = plugin name(s).
            'notice_can_activate_required'    => _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.', 'moving' ), // %1$s = plugin name(s).
            'notice_can_activate_recommended' => _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.', 'moving' ), // %1$s = plugin name(s).
            'notice_cannot_activate'          => _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.', 'moving' ), // %1$s = plugin name(s).
            'notice_ask_to_update'            => _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.', 'moving' ), // %1$s = plugin name(s).
            'notice_cannot_update'            => _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.', 'moving' ), // %1$s = plugin name(s).
            'install_link'                    => _n_noop( 'Begin installing plugin', 'Begin installing plugins', 'moving' ),
            'activate_link'                   => _n_noop( 'Begin activating plugin', 'Begin activating plugins', 'moving' ),
            'return'                          => esc_html__( 'Return to Required Plugins Installer', 'moving' ),
            'plugin_activated'                => esc_html__( 'Plugin activated successfully.', 'moving' ),
            'complete'                        => esc_html__( 'All plugins installed and activated successfully. %s', 'moving' ), // %s = dashboard link.
            'nag_type'                        => 'updated' // Determines admin notice type - can only be 'updated', 'update-nag' or 'error'.
        )
    );
    tgmpa( $plugins, $config );
}

?>