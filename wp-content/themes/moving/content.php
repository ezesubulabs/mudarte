<!-- blog -->
<a href="<?php the_permalink(); ?>" class="blog-big col-md-8 col-md-offset-2 bottom_60">
	<?php if(has_post_thumbnail()){ ?>
    <div class="image">
        <div class="icon">
            <i class="fa fa-picture-o" aria-hidden="true"></i>
        </div>
        <?php the_post_thumbnail(); ?>
    </div>
    <?php } ?>
    <div class="text">
        <h2><?php the_title(); ?></h2>
        <?php the_excerpt(); ?>
        <div class="info"><span><?php the_author(); ?></span> <?php esc_html_e('ON','moving'); ?> <?php the_time(get_option( 'date_format' )); ?></div>
    </div>
</a>
