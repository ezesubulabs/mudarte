<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<?php

$moving_custom  = moving_custom();

?>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">

	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <script type="text/javascript" src="https://cdn.conekta.io/js/latest/conekta.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD8sPSZlh6BE8GXLazZEZ9g_Q-LaaTPEjk&libraries=places"
            async defer></script>
    <?php if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) { ?>
	
		<?php if($moving_custom['favicon']['url']!=''){ ?>
			<link rel="icon" href="<?php echo esc_url($moving_custom['favicon']['url']); ?>" type="image/x-icon">
		<?php } ?>
		<?php if($moving_custom['apple_icon']['url']!=''){ ?>
		  <link rel="apple-touch-icon" href="<?php echo esc_url($moving_custom['apple_icon']['url']); ?>" />
		  <?php } ?>
		  <?php if($moving_custom['apple_icon_57']['url']!=''){ ?>
		  <link rel="apple-touch-icon" sizes="57x57" href="<?php echo esc_url($moving_custom['apple_icon_57']['url']); ?>">
		  <?php } ?>
		  <?php if($moving_custom['apple_icon_72']['url']!=''){ ?>
		  <link rel="apple-touch-icon" sizes="72x72" href="<?php echo esc_url($moving_custom['apple_icon_72']['url']); ?>">
		  <?php } ?>
		  <?php if($moving_custom['apple_icon_114']['url']!=''){ ?>
		  <link rel="apple-touch-icon" sizes="114x114" href="<?php echo esc_url($moving_custom['apple_icon_114']['url']); ?>">
		  <?php } ?>
	<?php } ?>
	<?php wp_head(); ?>

   
</head>
<body  <?php body_class();?>>

	<!-- Container -->
	<div id="container" >
		<!-- Header
		    ================================================== -->
		<header class="clearfix">
			<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
				<div class="container">
					<div class="top-line">
						<div class="row">
							<div class="col-md-8">
								<ul class="info-list">
									<?php if($moving_custom['phone']!=''){ ?>
									<li>
										<i class="fa fa-phone"></i>
										<?php esc_html_e('¿Necesitas ayuda?','moving'); ?> :
										<span><?php echo esc_html($moving_custom['phone']); ?></span>
									</li>
									<?php } ?>
									<?php if($moving_custom['time']!=''){ ?>
									<li>
										<i class="fa fa-clock-o"></i>
										<?php esc_html_e('Lun a Vie ','moving'); ?> :
										<span> <?php echo esc_html($moving_custom['time']);  ?></span>
									</li>
									<?php } ?>
								</ul>
							</div>	
							<div class="col-md-4">
								<ul class="social-icons">
									 <?php if($moving_custom['facebook']!=''){ ?>
				                     <li><a target="_blank" class="fb" href="<?php echo esc_url($moving_custom['facebook']); ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
				                    <?php } ?>
				                   <?php if($moving_custom['twitter']!=''){ ?>
				                     <li><a target="_blank" class="tw" href="<?php echo esc_url($moving_custom['twitter']); ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
				                    <?php } ?>
				                    <?php if($moving_custom['google-plus']!=''){ ?>
				                     <li><a target="_blank" class="gl" href="<?php echo esc_url($moving_custom['google-plus']); ?>"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                                    <?php } ?>
				                    <?php if($moving_custom['instagram']!=''){ ?>
				                     <li><a target="_blank" class="ins" href="<?php echo esc_url($moving_custom['instagram']); ?>"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
				                    <?php } ?>
				                    <?php if($moving_custom['behance']!=''){ ?>
				                     <li><a target="_blank" class="bh" href="<?php echo esc_url($moving_custom['behance']); ?>"><i class="fa fa-behance" aria-hidden="true"></i></a></li>
				                    <?php } ?>
				                    <?php if($moving_custom['dribbble']!=''){ ?>
				                     <li><a target="_blank" class="dr" href="<?php echo esc_url($moving_custom['dribbble']); ?>"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
				                    <?php } ?>
								</ul>
							</div>	
						</div>
					</div>
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="/home/#/" title="<?php bloginfo('name'); ?>"><?php if($moving_custom['logo']['url']!=''){ ?><img src="<?php echo $moving_custom['logo']['url']; ?>" alt="logo"><?php }else{ bloginfo('name'); } ?></a>
					</div>

					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav navbar-right">
							
							<li class="search"><a href="#" class="open-search"><i class="fa fa-search"></i></a>
								<form class="form-search" action="<?php echo esc_url( home_url('/') ); ?>" method="get">
									<input type="search" name="s"  placeholder="<?php esc_attr_e( 'Search', 'moving' ); ?>:"/>
									<button type="submit">
										<i class="fa fa-search"></i>
									</button>
								</form>
								
							</li>
						</ul>
						<?php 
							$menu_class = 'nav navbar-nav navbar-right';
							
							$defaults1= array(
								'theme_location'  => 'primary',
								'menu'            => '',
								'container'       => '',
								'container_class' => '',
								'container_id'    => '',
								'menu_class'      => $menu_class,
								'menu_id'         => '',
								'echo'            => true,
								 'fallback_cb'       => 'moving_bootstrap_navwalker::fallback',
								 'walker'            => new moving_bootstrap_navwalker(),
								'before'          => '',
								'after'           => '',
								'link_before'     => '',
								'link_after'      => '',
								'items_wrap'      => '<ul data-breakpoint="800" id="%1$s" class="%2$s">%3$s</ul>',
								'depth'           => 0,
							);
							if ( has_nav_menu( 'primary' ) ) {
								wp_nav_menu( $defaults1 );
							}
						
						?>
						
					</div><!-- /.navbar-collapse -->
				</div><!-- /.container-fluid -->
			</nav>
		</header>
		<!-- End Header -->